---
id: doc3
title: Specification
---


<br>
## 1. Rationale

For any student an important part of their study is their workplace. As such, our system will allow DETI students to find the room that at that time offers them the best study environment. Our system will have a base that will allow to offer concrete data to those responsible for the logistics of the rooms, giving concrete information of the level of use of the rooms of the department. This data can be used to consciously improve departmental infrastructure and offerings based on the habits and standards of your students. The data consists of the noise measured continuously by noise detection sensors installed in each room, that will later be processed in order to determine if such room is a suitable working and studying environment. The data will be visualized on the main dashboard of the project, that will display such measurements accordingly to each room, and raise informational messages depending on the same values. 

<br><br>
- - - -
<br>

## 2. Scenarios

### <b>Main scenario:</b>

Manel is a student at DETI in Universidade de Aveiro and every time he wants to find a room to study he has to go through multiple rooms, hoping to find one available and silent so he can focus and study properly. This task  can become annoying after a few tries without success. However, with “Shiuu” he can simplify this bothersome task by just accessing the platform dashboard and check which rooms and noisy and which ones are quiet, having a much relieving experience next time he wants to study at the department.

<br>

### <b>Scenario 01:</b> Visualization of room live information

The regular user of the platform wants to know on the fly what is the best room for him to study on the department, so he accesses the application dashboard in order to check what rooms are quiet and which ones are noisy.

<br>

### <b>Scenario 02:</b> Visualization of room history information

The regular user of the platform wants to know what rooms are generally the most noisy and which ones are the quietest, so he accesses the application dashboard and checks the room history values.

<br>

### <b>Scenario 03:</b> Management of the platform

The System Administrator of the platform wants to add or remove rooms or sensors from the database, so he accesses the dashboard, after login with his administrator credentials, and selects the operation intended.

<br>

### <b>Scenario 04:</b> Visualization of the general department room information

The department employee wants to manage it, so he accesses the dashboard of the application in order to visualize the general condition of the rooms.
<br><br><br>
- - - -
<br>

## 3. Features and Description

When it comes to the system features available on the platform, these are: 

* <b>FE0 - Add rooms (sysAdmin)</b>

	An admin type of user can add rooms to the system as long as they have the required sensors previously installed. New rooms information will be visualized on the dashboard of the platform.

* <b>FE1 - Remove rooms (sysAdmin)</b>

	An admin type of user can remove rooms from the system. Removed rooms may still contain the sensors but their information will no longer be visualized on the dashboard.

* <b>FE2 - Select room</b>

	Users can select any available room on the dashboard as long as it has an installed audio sensor.

* <b>FE3 - Visualize room information</b>

	Both type of users can at any given time visualize the information regarding the selected room.

* <b>FE4 - Login</b>

	In order to access the platform all users must go through a login authentication process by inserting their username/email and respective password.

* <b>FE5 - Register</b>

	Regular users must have an account on the platform and thus must fulfill the registration form in order to create an account. 
<br><br><br>
- - - -
<br>

## 4. Use cases

Regarding the use cases, they are applied to two different type of users of the application (the actors): the regular user of the system (which can be a student or a DETI employee) and the sysAdmin (who is a regular user but has increased permissions and features available).<p>
The use cases are as follows:

* <b>UC01:</b> Check available rooms live information
* <b>UC02:</b> Check rooms statistics and history
* <b>UC03:</b> Monitor the department rooms
* <b>UC04:</b> Manage the platform
* <b>UC05:</b> Login
* <b>UC06:</b> Register

<br> 
![picture alt](https://imgur.com/pDQxjVj.png "usecase")


<br> <br>
- - - -
<br>

## 5. Scenario testing

To ensure that the application is working as intended for the previously defined scenarios and use cases, some test scenarios have been created (design testing):

* <b>Test Scenario 01:</b> Check the Login and Authentication Functionality
* <b>Test Scenario 02:</b> Check live sensor information can be visualized and is updated on a time basis.
* <b>Test Scenario 03:</b> Check sensor information is being stored by visualizing the history.
* <b>Test Scenario 04:</b> Check if System Admin can add or remove sensors and rooms from the platform.
* <b>Test Scenario 05:</b> Check if the user can login in the platform.
* <b>Test Scenario 06:</b> Check if the user can create an account in the platform.

<br>

In order to ensure the acceptance criteria of the test scenarios, Behavior Driven Development  (BDD) software testing process was used based on the previously established use cases:

* <b>UC-1 :   Check available rooms live information</b>

	<b>Given</b> I open the dashboard <p>
	<b>When</b> I select a given room<p>
	<b>Then</b> I see the noise information of that room

* <b>UC-2 :   Check rooms statistics and history</b>

	<b>Given</b> I open the dashboard <p>
	<b>When</b> I select a given room<p>
	<b>Then</b> I see the noise history information of that room

* <b>UC-3:    Monitor the department rooms</b>

	<b>Given</b> I open the dashboard <p>
	<b>When</b> select a given room <p>
	<b>Then</b> I see the the real time information of that room

* <b>UC-5:    Login</b>

	<b>Given</b> I open the dashboard <p>
	<b>When</b> I input my login credentials<p>
	<b>Then</b> I see a notification that login was successful

* <b>UC-6    Register</b>

	<b>Given</b> I open the dashboard <p>
	<b>When</b> I properly fill in the register form <p>
	<b>Then</b> I see a notification that register was successful
<br><br><br>
- - - -
<br>

## 6. Requirements

* Each room requires at least one properly installed noise sensor with internet access in order to  communicate with its endpoint.
* Web browser of the user aiming to  access the application dashboard
* User authentication for regular type of users in order to use API and access dashboard contents
* Main server with public internet access.
* One sysadmin charged with managing the platform, as well as maintaining the good operability and status of the sensors.

 <br>
- - - -
<br>

## 7. Data Model

In this section it is described the concepts and relations used in the high level representation of the platform data model. Each room contains noise detection sensor(s), strategically placed in the room in order to properly record the noise occurring in it. For better results some rooms may need additional sensors as for example the amphitheaters of the department, due to their increased area.<p>
For each room it is recorded its type (standard room / amphitheater / etc), its room number (ex. 218 (room nº 18 at second floor)), administrator responsible for the room and number of sensors it has installed.<p>
Users of the system are defined by their credentials of access (username/password) and type of user (normal / admin).<p>
Each sensor is defined by their respective id, responsable admin and room it is currently located.

<br>

### <b>Events:</b>

Event occur in the following circumstances:
* Continuously at the sensors by generating and sending data information;
* Continuously at the broker by receiving data information form sensors or users;
* When a user logins on the platform (validating/rejecting user);
* When a user creates an account on the platform;
* When a user selects/visualizes a given room related information;
* Whenever a sysAdmin adds/removes rooms from the platform.
* Sensor/producer transmitted information (as data stream):
	* Department room number (Int);
	* Sensor id (Int);
	* Time (Time);
	* Date (Date);
	* Noise values (Int, measured in decibel).
* Internal system/consumer obtained information (at broker):
	* Department room number (Int);
	* Time (Time);
	* Date (Time);
	* Noise values (Int,measured in decibel);
	* User login/register credentials (String).

A representation of our system data model can be visualized in the following data model UML diagram:

<br> 

![picture alt](https://imgur.com/aZ9fp2h.png "DataModel")

<br>

### <b>Sensor/producer transmitted information (as data stream):</b>

* Department room number (Int);
* Sensor id (Int);
* Time (Time);
* Date (Date);
* Noise values (Int, measured in decibel).

<br>

### <b>Internal system/consumer obtained information (at broker):</b>

* Department room number (Int);
* Time (Time);
* Date (Time);
* Noise values (Int,measured in decibel);
* User login/register credentials (String).

<br><br>
- - - -
<br>

## 8. Others

### <b>Risks:</b>

As it happens with every system in or post development there are always some risks involved that should be listed and classified, whether they are relevant to the core principle of the project or not. The following risks were identified as the most harmful ones regarding our platform:

* Power outage on a sensor, destabilizing the flow of information;
* Power outage of the server, shutting down the platform;
* High internet delays or even complete connection failure;
* User information leaks.

<br>

### <b>Challenges:</b>

No project is developed from start to finish without facing some challenges that need to be overcomed for good system operability. As such the following were identified: 

* Operability, no delays, real time information processing and display;
* Stability of sensors communication
* Maintenance of sensors, multiple sensors spreaded through the department.

<br>

### <b>Business Logic:</b>

Since the aim of this project is not the development of a platform with intentions of making profit, business logic is not very applicable here. Our aim is to develop a platform to help students find the best workplace in the department with no costs involved on their part. The servers would be hosted and deployed in university given machines. However it is meant to behave and act as a professional business enterprise software platform, and as such must meet its requirements and demands, with the ambition of potentially scale the platform into a real live system and potential revenue.



<br><br>



