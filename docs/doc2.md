---
id: doc2
title: Developer Area
---
<br>
##   1. System endpoint (API) 

Regarding the application endpoint interface (also known as API), it contains the following RESTful HTTP methods implemented:

* <b>a. Retrieve users of the platform</b>	
	* i.  Name: users
	* ii. Type: GET

* <b>b. Retrieve single user </b>	
	* i.  Name: user
	* ii. Type: GET
	* iii. Param: User Id

* <b>c. Delete user </b>	
	* i.  Name: delUser
	* ii. Type: DELETE
	* iii. Params: User Id

* <b>d. Retreive sensors</b>	
	* i.  Name: sensors
	* ii. Type: GET
	* iii. Param: Sensor Id

* <b>e. Create new sensor</b>	
	* i.  Name: createSensor
	* ii. Type: POST
	* iii. Params: Type, Id

* <b>f. Delete existing sensor</b>	
	* i.  Name: delSensor
	* ii. Type: DELETE
	* iii. Params: Sensor Id

* <b>g. Update existing sensor</b>	
	* i.  Name: updatelSensor
	* ii. Type: PUT
	* iii. Params: Sensor Id, Sensor Type

* <b>h. Retrieve room sensors</b>	
	* i.  Name: sensors
	* ii. Type: GET
	* iii. Params: Room Id 

* <b>i. Add new room</b>	
	* i.  Name: createRoom
	* ii. Type: POST
	* iii. Params: Room Name, Room Location, Sensor Id 

* <b>j. Delete existing room</b>	
	* i.  Name: delRoom
	* ii. Type: DELETE
	* iii. Params: Room Id

* <b>k. Update existing room</b>	
	* i.  Name: editRoom
	* ii. Type: PUT
	* iii. Params: Room Id, Room Name, Room Location, Sensor Id


<br> 

- - - -

<br>

##  2. System Deployment 

In order to deploy our system the following steps must be followed and its respective dependencies properly installed:

* <b>Client Web App:</b>

```
	$yarn install
	$json-server -p 1337 --watch api.json
	$yarn start
```

* <b>Noise Generators:</b>
```
$ sudo apt install virtualenv   # Install virtualenv
$ virtualenv -p python3.5 venv  # Creation of the virtual environment
$ source ./venv/bin/activate    # Load the virtual environment
```

* <b>Database</b>
	* Persistance module:

	```
	$ docker run --name mongo -p 27017:27017 -d mongo --smallfiles
	```

	* Connector:

	```
	$ docker build -t connect_kc .
	$ docker run -idt --rm --name connect_kc --link mongo:mongo
	```

* <b>System endpoint (API):</b>

	```
	$ docker run -p 8080:8080 --rm --interactive registry.gitlab.com/merrdaa/mcon:latest
	$ docker run -d -p 2181:2181 --name zookeeper jplock/zookeeper
	$ docker run -d -p 9092:9092 --name kafka --link zookeeper:zookeeper ches/kafka
	```

* <b>Stream processor:</b>

	```
	$ mvn clean package
	$ mvn exec:java -Dexec.mainClass=streamApps.DetectNoise
	```

<br> 

- - - -
<br>

## 3. System monitoring tools

In order to monitor the system and track the logging analytics in real time, ELK stack (now ElasticStack) has been implemented. The stack is composed by three major components: ElasticSearch, Kibana and Logstash. 

<br> 

- - - -

<br>

##  4. System monitoring tools 

In order to automate the system development with continuous integration and facilitating several aspects of the delivery as version control, Jenkins automated server was implemented. 


<br><br>


