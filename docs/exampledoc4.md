---
id: doc4
title: User Manual
---
<br>
In this section it is presented a simple manual with visual representation of the dashboard, that users can follow in order to understand the functionality of our system.
<br><br>
## 1. Login Page 

The first page a user sees when accessing our platform is the login page. Here the user is asked to insert his credentials in order to proceed.
<br><br> 
![picture alt](https://imgur.com/7908CKY.png "login")

<br><br>
- - - -
<br>

## 2. Dashboard 

Upon successful authentication the user is then redirected to the main page of the application, the dashboard. Here it is displayed the live visualization of the noise values of a given room as well as the history of recorded values of the same room. When a given room sensor produces a noise value loud enough an alert will be triggered and visualized here as well.

<br>
![picture alt](https://imgur.com/f8RmLTa.png "dashboard")

<br><br>
- - - -
<br>

## 3. Sensors list 

In this section the user can visualize the list of sensors that are currently existent in the system as well as its corresponding details. Here it is also possible to add new sensors to the list of sensors or edit an existing sensor instead in case of the user is an admin of the system. 
<br><br>
![picture alt](https://imgur.com/muNUT8P.png "sensorslist")

<br><br>
- - - -
<br>

## 4. Sensor editing 

Here the user can edit a selected sensor by redefining its type of sensor or its id. User can also delete said sensor.
<br><br>
![picture alt](https://imgur.com/9lARpcf.png "editing")
<br><br>
- - - -
<br>

## 5. Room list 

In this section the user can visualize the list existing rooms, including their specifications, which include their ID, room name (given by its number), location (university department they are located) and the sensors they contain. Here it is also possible to edit or add new rooms.
<br><br>

![picture alt](https://imgur.com/10ywF8G.png "roomlist")
<br><br>
- - - -
<br>


## 6. User list 

In this section the user can visualize the list of users that are currently registered in the system as well as its corresponding details, which include ID, name, email and type of user which can be regular user, with normal permissions of access, or admin with no restrictions of access. 
<br><br>
![picture alt](https://imgur.com/ATXiRBN.png "userlist")


<br><br>









