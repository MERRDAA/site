---
id: doc1
title: Architecture Description
sidebar_label: Architecture
---

<br>

Here is presented and described the architecture components of the platform, giving a better perception of the system as a whole as well as of each individual module. It is an architecture similar to most IoT like systems composed of 4 main components: <b>multiple sensors</b>, a <b>data collector</b>, a <b>data processor</b> and a <b>dashboard</b>.
<br><br><br>
![picture alt](https://i.imgur.com/Si6oaBp.png "Diagram")
<br><br>
- - - -
<br>
## Architecture Components 

The architecture is composed by the following software components:

* <b>i. Sensor simulation module</b>

	Real time noise values generation. The purpose of this module is to simulate a real noise capture sensor by generating random decibel values within a certain interval, which are then sent into the broker module, where it will be processed accordingly.


* <b>ii. Broker module </b>	

	Consumer entity that receives the information sent unidirectional from the sensors. Contains two kafka topics : “noise” and “quiet”.


* <b>iii. Stream processor module</b>	

	Processor entity of the architecture that reads from the topics and stores the data streams to the database and displays alerts on the dashboard.


* <b>iv. Persistence module</b>	

	Storage of the noise values after proper reading and processing. These values can then be accessed by the users when previewing the information in the main dashboard.


* <b>v. System endpoint (API)</b>

	Module responsible for retrieving the persistence data from the database module and displaying it at the interface module.


* <b>vi. User interface module</b>

	The dashboard that displays the live reading values and the database history stored values.
<br><br><br>
- - - -
<br>
## System Features 

When it comes to the system features they are all represented by the front-end of the platform. As such, they can be described as follows:

* <b>i. FE0 - Add rooms (sysAdmin)</b>

	An admin type of user can add rooms to the system as long as they have the required sensors previously installed. New rooms information will be visualized on the dashboard of the platform.

* <b>ii. FE1 - Remove rooms (sysAdmin)</b>

	An admin type of user can remove rooms from the system. Removed rooms may still contain the sensors but their information will no longer be visualized on the dashboard.

* <b>iii. FE2 - Select room</b>

	Users can select any available room on the dashboard as long as it has an installed audio sensor.

* <b>iv. FE2 - Visualize room information</b>

	Both type of users can at any given time visualize the information regarding the selected room.

* <b>v. FE3 - Login </b>

	In order to access the platform all users must go through a login      authentication process by inserting their username/email and respective password.

* <b>vi. FE4 - Register</b>

	Regular users must have an account on the platform and thus must fulfill the registration form in order to create an account.
<br><br>