# Documentation Website

### Links to study

https://docusaurus.io/docs/en/installation.html

### Local Development

```
$ cd website
$ yarn start
```

### Deploy to xcoa

```
$ ./bin/deploy
```
