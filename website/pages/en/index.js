/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

const CompLibrary = require('../../core/CompLibrary.js');
const MarkdownBlock = CompLibrary.MarkdownBlock; /* Used to read markdown */
const Container = CompLibrary.Container;
const GridBlock = CompLibrary.GridBlock;

const siteConfig = require(process.cwd() + '/siteConfig.js');

function imgUrl(img) {
  return siteConfig.baseUrl + 'img/' + img;
}

function docUrl(doc, language) {
  return siteConfig.baseUrl + 'docs/' + (language ? language + '/' : '') + doc;
}

function pageUrl(page, language) {
  return siteConfig.baseUrl + (language ? language + '/' : '') + page;
}

class Button extends React.Component {
  render() {
    return (
      <div className="pluginWrapper buttonWrapper">
        <a className="button" href={this.props.href} target={this.props.target}>
          {this.props.children}
        </a>
      </div>
    );
  }
}

Button.defaultProps = {
  target: '_self',
};

const SplashContainer = props => (
  <div className="homeContainer">
    <div className="homeSplashFade">
      <div className="wrapper homeWrapper">{props.children}</div>
    </div>
  </div>
);

const Logo = props => (
  <div className="projectLogo">
    <img src={props.img_src} />
  </div>
);

const ProjectTitle = props => (
  <h1 className="projectTitle">
    {siteConfig.title}
    <small>{siteConfig.tagline}</small>
  </h1>
);

const PromoSection = props => (
  <div className="section promoSection">
    <div className="promoRow">
      <div className="pluginRowBlock">{props.children}</div>
    </div>
  </div>
);

class HomeSplash extends React.Component {
  render() {
    let language = this.props.language || '';
    return (
      <SplashContainer>
        <div className="inner">
          <ProjectTitle />
          <PromoSection>
            <Button href="#Description">What it is?</Button>
            <Button href={docUrl('doc1.html', language)}>Documentation</Button>
            <Button href={pageUrl('help.html', language)}>Team</Button>
          </PromoSection>
        </div>
      </SplashContainer>
    );
  }
}

const Block = props => (
  <Container
    padding={['bottom', 'top']}
    id={props.id}
    background={props.background}>
    <GridBlock align="center" contents={props.children} layout={props.layout} />
  </Container>
);

const Features = props => (
  <Block  background="dark" layout="twoColumn">
    {[
      {
        image: siteConfig.baseUrl + 'img/student.png',
        imageAlign: 'top',
        imageAlt: 'Icon made by prosymbols from www.flaticon.com ',
        content: 'Provides the students with tools to find the perfect environment to study',
        title: 'For Students',
      },
      {
        image: siteConfig.baseUrl + 'img/laptop.png',
        imageAlign: 'top',
        imageAlt: 'Icon made by prosymbols from www.flaticon.com ',
        content: 'Allows department employees to close monitor the noise in classrooms',
        title: 'For Employees',
      },
    ]}
  </Block>
);

const Problem = props => (
  <Block>
    {[
      {
        image: siteConfig.baseUrl + 'img/studying.gif',
        imageAlign: 'right',
        content: 'We are a team of students that likes the good old silence while we study. Unfortunately, in our department, silence is not always the default mode people tend to have. Our problem is to find the perfect place to study.',
        title: 'The Problem and Context',
      },
    ]}
  </Block>
)

const Solution = props => (
  <Block background="dark">
    {[
      {
        image: siteConfig.baseUrl + 'img/shutup.gif',
        imageAlign: 'left',
        content: 'Shiuu is a concept that that uses a data collecting system that reacts upon changes in the environment, case in point, noise. Students will now be able to know which rooms are adequate for studying and those who are being loudmouths will get visual warnings to lessen that behavior.',
        title: 'Our Solution',
      },
    ]}
  </Block>
)

const FeatureCallout = props => (
  <div
    className="productShowcaseSection paddingBottom"
    style={{textAlign: 'center'}}>
    <h2>Feature Callout</h2>
    <MarkdownBlock>These are features of this project</MarkdownBlock>
  </div>
);

const LearnHow = props => (
  <Block background="light">
    {[
      {
        content: 'Talk about learning how to use this',
        image: imgUrl('shiu.svg'),
        imageAlign: 'right',
        title: 'Learn How',
      },
    ]}
  </Block>
);

const TryOut = props => (
  <Block id="try">
    {[
      {
        content: 'Talk about trying this out',
        image: imgUrl('shiu.svg'),
        imageAlign: 'left',
        title: 'Try it Out',
      },
    ]}
  </Block>
);

const smallDescription = props => (
  <Block id="Description">
    {[
      {

        content: 'For any student an important part of their study is their workplace. As such, our system will allow DETI students to find the room that at that time offers them the best study environment. Our system will have a base that will allow to offer concrete data to those responsible for the logistics of the rooms, giving concrete information of the level of use of the rooms of the department. This data can be used to consciously improve departmental infrastructure and offerings based on the habits and standards of your students. Through multiple sensors, data will be intuitively illustrated in our platform so the users can get a good grasp as of what rooms are available and at what conditions they are, live time.',
        title: 'Description',
      },
    ]}
  </Block>
);

const Showcase = props => {
  if ((siteConfig.users || []).length === 0) {
    return null;
  }
  const showcase = siteConfig.users
    .filter(user => {
      return user.pinned;
    })
    .map((user, i) => {
      return (
        <a href={user.infoLink} key={i}>
          <img src={user.image} title={user.caption} />
        </a>
      );
    });

  return (
    <div className="productShowcaseSection paddingBottom">
      <h2>{"Who's Using This?"}</h2>
      <p>This project is used by all these people</p>
      <div className="logos">{showcase}</div>
      <div className="more-users">
        <a className="button" href={pageUrl('users.html', props.language)}>
          More {siteConfig.title} Users
        </a>
      </div>
    </div>
  );
};

class Index extends React.Component {
  render() {
    let language = this.props.language || '';

    return (
      <div>
        <HomeSplash language={language} />
        <smallDescription/>
        <Features/>
        <Problem/>
        <Solution/>
      </div>
    );
  }
}

module.exports = Index;
