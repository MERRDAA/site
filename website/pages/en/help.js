const React = require('react');

const CompLibrary = require('../../core/CompLibrary.js');
const Container = CompLibrary.Container;
const GridBlock = CompLibrary.GridBlock;

const siteConfig = require(process.cwd() + '/siteConfig.js');

function imgUrl(img) {
  return siteConfig.baseUrl + 'img/' + img;
}

class Help extends React.Component {
  render() {
    const devs = [
      {
        content: 'carlos.paredes@ua.pt - 76585',
        image: siteConfig.baseUrl + 'img/Soeima.PNG',
        imageAlign: 'bottom',
        imageAlt: 'Carlos Soeima',
        title: 'Carlos Soeima',
      },
      {
        content: 'joaop.amaral@ua.pt - 76585',
        image: siteConfig.baseUrl + 'img/amaral.jpg',
        imageAlign: 'bottom',
        imageAlt: 'João Amaral',
        title: 'João Amaral',
      },
      {
        content: 'joaobranquinho@ua.pt - 76585',
        image: siteConfig.baseUrl + 'img/Branco.PNG',
        imageAlign: 'bottom',
        imageAlt: 'João Branquinho',
        title: 'João Branquinho',
      },
      {
        content: 'lfssilva@ua.pt - 76585',
        image: siteConfig.baseUrl + 'img/luis.jpg',
        imageAlign: "bottom",
        imageAlt: 'Luís Silva',
        title: 'Luís Silva',
      },
      {
        content: 't.ramalho22@ua.pt - 76585',
        image: siteConfig.baseUrl + 'img/Ramalho.PNG',
        imageAlign: "bottom",
        imageAlt: "Tiago Ramalho",
        title: 'Tiago Ramalho',
      },
      {
        content: ' ',
        title: ' ',
      },
    ];

    return (
      <div className="docMainWrapper wrapper">
        <Container className="mainContainer documentContainer postContainer">
          <div className="post">
            <header className="postHeader">
              <h2>Need help?</h2>
            </header>
            <p>This project is maintained by a dedicated group of people.</p>
            <p>You can find the hours which one of us spent <a href="https://goo.gl/rcp6eL">here</a> </p>
            <GridBlock align="center" contents={devs} className="devs" layout="threeColumn"/>
          </div>
        </Container>
      </div>
    );
  }
}

module.exports = Help;
