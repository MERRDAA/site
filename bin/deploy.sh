#! /usr/bin/env bash

echo -e "\x1B[32mBuilding site...\x1B[0m"

cd ./website

yarn build

if [ $? -ne 0 ]; then
    exit 1
fi


echo -e "\x1B[32mDeploying to xcoa.av.it...\x1B[0m"

rsync -crvz --rsh='ssh' --delete-after --delete-excluded   ./build/~es2017-2018_g401/* es2017-2018_g401@xcoa.av.it.pt:public_html/

if [ $? -ne 0 ]; then
    exit 1
fi

echo -e "\x1B[32mDone!\x1B[0m"
